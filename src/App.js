import { Container } from "@mui/material";
import UserDatatable from "./components/UserDatatable";

function App() {
  return (
    <Container>
      <UserDatatable/>
    </Container>
  );
}

export default App;
