import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE
} from "../constants/user.constant";

//các state sẽ thay đổi 
const taskState = {
    users: [],
    pending: false,
    error: null,
    currentPage: 1,
    limit: 3,
    noPage: 0
};

const taskReducer = (state = taskState, action) => {
    //khi giá trị được lấy từ view
    switch (action.type) {
        case USERS_FETCH_PENDING:
            state.pending = true;
            break;
        case USERS_FETCH_SUCCESS:
            state.pending = false;
            state.noPage = Math.ceil(action.totalUser / state.limit);
            state.users = action.data.slice((state.currentPage-1)*state.limit, (state.currentPage*state.limit));
            break;
        case USERS_FETCH_ERROR:
            break;
        case USERS_PAGE_CHANGE:
            state.currentPage = action.page;
            break;
        default:
            break;
    }

    //state lưu giá trị mới
    return { ...state };
};

export default taskReducer;