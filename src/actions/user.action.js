import axios from "axios";
import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE
} from "../constants/user.constant";

const url = "http://203.171.20.210:8080/devcamp-register-java-api/users";

const axiosCall = async (url, body) => {
    const response = await axios(url, body);
    return response.data;
};

export const fetchUsers = (page, limit) => {
    return async (dispatch) => {

        await dispatch({
            type: USERS_FETCH_PENDING
        })

        const responseTotalUser = await fetch(url);

        const dataTotalUser = await responseTotalUser.json();

        const params = new URLSearchParams({
            _start: (page-1) * limit, 
            _limit: limit
        });

        axiosCall(url+"?"+params.toString())
            .then(result => {
                console.log(params.toString());
                return dispatch({
                    type: USERS_FETCH_SUCCESS,
                    totalUser: dataTotalUser.length,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: USERS_FETCH_ERROR,
                    error: error
                })
            });

    }
}

export const pageChangePagination = (page) => {
    return {
        type: USERS_PAGE_CHANGE,
        page: page
    }
}