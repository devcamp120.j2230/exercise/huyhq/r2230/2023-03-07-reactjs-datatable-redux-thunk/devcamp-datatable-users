import { Container, CircularProgress, Grid, TableContainer, TableHead, TableRow, TableCell, TableBody, Button, Paper, Table, Pagination } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { fetchUsers, pageChangePagination } from "../actions/user.action";

const UserDatatable = () => {
    const dispatch = useDispatch();

    const { users, currentPage, limit, noPage, pending } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    });

    useEffect(() => {
        // Gọi API để lấy dữ liệu
        dispatch(fetchUsers(currentPage, limit));
    }, [currentPage]);

    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }

    return (
        <Container maxWidth={false} disableGutters>
            {
                pending
                    ?
                    <Grid container justifyContent='center'>
                        <CircularProgress />
                    </Grid>
                    :
                    <Grid container justifyContent='center'>
                        <Grid item sx={12}>
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Id</TableCell>
                                            <TableCell align="right">Firstname</TableCell>
                                            <TableCell align="right">Lastname</TableCell>
                                            <TableCell align="right">Country</TableCell>
                                            <TableCell align="right">Subject</TableCell>
                                            <TableCell align="right">Custom Type</TableCell>
                                            <TableCell align="right">Register Status</TableCell>
                                            <TableCell align="right">Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {users.map((row) => {
                                            return <TableRow
                                                key={row.id}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                            >
                                                <TableCell component="th" scope="row">
                                                    {row.id}
                                                </TableCell>
                                                <TableCell align="right">{row.firstname}</TableCell>
                                                <TableCell align="right">{row.lastname}</TableCell>
                                                <TableCell align="right">{row.country}</TableCell>
                                                <TableCell align="right">{row.subject}</TableCell>
                                                <TableCell align="right">{row.customerType}</TableCell>
                                                <TableCell align="right">{row.registerStatus}</TableCell>
                                                <TableCell align="right">
                                                    <Button variant="contained">Detail</Button>
                                                </TableCell>
                                            </TableRow>
                                        })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                        <Grid item lg={12} md={12} sm={12} xs={12} mt={5}>
                            <Pagination count={noPage} page={currentPage} onChange={onChangePagination} />
                        </Grid>
                    </Grid>
            }
        </Container>
    )
}

export default UserDatatable;