import { combineReducers } from "redux";
import { createStore, applyMiddleware } from "redux";
import taskReducer from "../reducers/task.reducer";
import thunk from "redux-thunk";

//root chứa các task reducer 
const rootReducer = combineReducers({
    //gọi các task
    taskReducer
});

//tạo store
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;